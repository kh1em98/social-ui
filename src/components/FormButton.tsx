import React, { FC } from 'react';
import { Button } from '@chakra-ui/react';

export const FormButton = ({ loading, disabled, ...otherProps }) => {
  const { children } = otherProps;

  return (
    <Button {...otherProps} isDisabled={disabled} isLoading={loading}>
      {children}
    </Button>
  );
};
