import React, { FC, ReactNode } from 'react';
import classnames from 'classnames';

import { Field, FieldProps, FieldAttributes } from 'formik';
import { CheckIcon } from '@chakra-ui/icons';
import {
  InputGroup,
  InputLeftElement,
  Input,
  InputRightElement,
  Textarea,
  Select,
  FormControl,
  FormHelperText,
  FormLabel,
  FormErrorMessage,
} from '@chakra-ui/react';

interface Props extends FieldAttributes<any> {
  label?: string;
  icon?: string;
  required?: boolean;
  tag?: string;
}

export const FormField: FC<Props> = ({
  label,
  icon,
  required,
  tag,
  helperText,
  ...otherProps
}) => {
  const { children } = otherProps;
  return (
    <FormControl mt={5}>
      <Field {...otherProps} required={required}>
        {(props: FieldProps): ReactNode => {
          const className = classnames(
            'form-control',
            { 'is-invalid': !!props.meta.error },
            otherProps.className,
          );
          const error = props.meta.error ? (
            <FormErrorMessage>{props.meta.error}</FormErrorMessage>
          ) : null;

          const textHelper = helperText ? (
            <FormHelperText>{helperText}</FormHelperText>
          ) : null;

          switch (tag) {
            case 'select':
              return (
                <>
                  <Select
                    placeholder="Select option"
                    className={className}
                    {...props.field}
                    {...otherProps}>
                    {children}
                  </Select>

                  {textHelper}
                  {error}
                </>
              );
            case 'textarea':
              return (
                <>
                  <Textarea
                    className={className}
                    {...props.field}
                    {...otherProps}>
                    {children}
                  </Textarea>
                  {textHelper}
                  {error}
                </>
              );
            default:
              return (
                <>
                  {icon ? (
                    <>
                      <FormLabel>
                        {label && (
                          <label>{`${label}${required ? ' *' : ''}`}</label>
                        )}
                      </FormLabel>
                      <InputGroup>
                        <InputLeftElement
                          pointerEvents="none"
                          color="gray.300"
                          fontSize="1.2em"
                          children="$"
                        />
                        <Input
                          className={className}
                          {...props.field}
                          {...otherProps}
                        />
                      </InputGroup>

                      {textHelper}
                      {error}
                    </>
                  ) : (
                    <>
                      <InputGroup>
                        <InputLeftElement
                          pointerEvents="none"
                          color="gray.300"
                          fontSize="1.2em"
                          children="$"
                        />
                        <Input
                          className={className}
                          {...props.field}
                          {...otherProps}
                        />
                        <InputRightElement
                          children={<CheckIcon color="green.500" />}
                        />
                      </InputGroup>

                      {textHelper}
                      {error}
                    </>
                  )}
                </>
              );
          }
        }}
      </Field>
    </FormControl>
  );
};
