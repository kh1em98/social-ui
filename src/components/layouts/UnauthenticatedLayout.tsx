import { Container } from '@chakra-ui/react';
import NavBar from '../Navbar';
import React, { ReactNode } from 'react';

export const UnauthenticatedLayout = ({
  children,
}: {
  children: ReactNode;
}) => (
  <>
    <NavBar />
    <Container p={10}>{children}</Container>
  </>
);
