interface ServerErrorBody {
  error: {
    statusCode?: string | number;
    name?: string;
    message?: string;
    code?: string;
    details?: any;
  };
}

export const getErrorCode = (error): string => {
  if (error.message === 'Network Error') {
    return 'networkError';
  }

  if (error.response?.data?.error) {
    const serverErrorBody = error.response?.data as ServerErrorBody;
    const key = serverErrorBody.error.message;

    if (/^\w+$/.test(key!)) {
      return key!;
    }

    return serverErrorBody.error.code!;
  }

  return error.message;
};
