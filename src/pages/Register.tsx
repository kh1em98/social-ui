import React, { FC } from 'react';
import { Heading, useToast } from '@chakra-ui/react';
import { Formik, FormikProps, FormikHelpers } from 'formik';
import { useTranslation } from 'react-i18next';

import { UnauthenticatedLayout } from '../components/layouts/UnauthenticatedLayout';
import { FormField } from '../components/formik/FormField';
import { SubmitButton } from '../components/formik/SubmitButton';
import { AppDispatch } from '../redux/store';
import { useDispatch } from 'react-redux';
import { loginWithEmail } from '../redux/slices/authSlice';
import { useHistory } from 'react-router-dom';
import { getErrorMessageCode } from '../view-models/Error';

export interface RegisterForm {
  email: string;
  password: string;
  firstName: string;
  lastName: string;
}

const Register: FC = () => {
  const { t } = useTranslation();
  const history = useHistory();
  const dispatch: AppDispatch = useDispatch();
  const toast = useToast();

  const handleRegister = async (
    values: RegisterForm,
    actions: FormikHelpers<RegisterForm>,
  ) => {
    actions.setSubmitting(true);
    try {
      const user = await dispatch(loginWithEmail(values));

      if (user) {
        history.push('/');
      }
    } catch (e) {
      actions.setSubmitting(false);

      toast({
        title: "Error",
        description: getErrorMessageCode(e, {
          VALIDATION_FAILED: 'incorrectEmailOrPassword',
          invalid_credentials_email: 'incorrectEmailOrPassword',
        }),
        status: "error",
        isClosable: true,
      })

    }
  };
  return (
    <UnauthenticatedLayout>
      <Formik
        initialValues={{
          email: '',
          password: '',
          firstName: '',
          lastName: ''
        }}
        onSubmit={handleRegister}>
        {(props: FormikProps<RegisterForm>): JSX.Element => (
          <form onSubmit={props.handleSubmit}>
            <Heading as="h2" size="xl">
              Register
            </Heading>
            <FormField
              name="firstName"
              placeholder={t('First name')}
              icon="cil-user"
              type="text"
              label="First name"
            />
            <FormField
              name="lastName"
              placeholder={t('Last name')}
              icon="cil-user"
              type="text"
              label="Last name"
            />


            <FormField
              name="email"
              placeholder={t('email')}
              icon="cil-user"
              type="email"
              label="Email"
              helperText="We will never share your email"
            />

            <FormField
              name="password"
              placeholder={t('password')}
              icon="cil-lock-locked"
              type="password"
              label="Password"
            />

            <SubmitButton mt={5} colorScheme="blue" isFullWidth>
              Register
            </SubmitButton>
          </form>
        )}
      </Formik>
    </UnauthenticatedLayout>
  );
};

export default Register;



{/* <UnauthenticatedLayout>
  <Heading as="h2" size="xl">
    Register
  </Heading>
  <FormControl id="fullName" mt={5}>
    <FormLabel>Name</FormLabel>
    <Input type="text" />
  </FormControl>

  <FormControl id="email" mt={5}>
    <FormLabel>Email address</FormLabel>
    <Input type="email" />
    <FormHelperText>We'll never share your email.</FormHelperText>
  </FormControl>

  <FormControl id="password" mt={3}>
    <FormLabel>Password</FormLabel>
    <Input type="password" />
  </FormControl>
  <Button mt={5} isLoading={false} colorScheme="blue" isFullWidth>
    Register
  </Button>
</UnauthenticatedLayout> */}
