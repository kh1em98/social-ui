import React, { FC } from 'react';
import { Heading, useToast } from '@chakra-ui/react';
import { Formik, FormikProps, FormikHelpers } from 'formik';
import { useTranslation } from 'react-i18next';

import { UnauthenticatedLayout } from '../components/layouts/UnauthenticatedLayout';
import { FormField } from '../components/formik/FormField';
import { SubmitButton } from '../components/formik/SubmitButton';
import { AppDispatch } from '../redux/store';
import { useDispatch } from 'react-redux';
import { loginWithEmail } from '../redux/slices/authSlice';
import { useHistory } from 'react-router-dom';
import { getErrorMessageCode } from '../view-models/Error';

export interface LoginForm {
  email: string;
  password: string;
}

const Login: FC = () => {
  const { t } = useTranslation();
  const history = useHistory();
  const dispatch: AppDispatch = useDispatch();
  const toast = useToast();

  const handleLogin = async (
    values: LoginForm,
    actions: FormikHelpers<LoginForm>,
  ) => {
    actions.setSubmitting(true);
    try {
      const user = await dispatch(loginWithEmail(values));

      if (user) {
        history.push('/');
      }
    } catch (e) {
      actions.setSubmitting(false);

      toast({
        title: "Error",
        description: getErrorMessageCode(e, {
          VALIDATION_FAILED: 'incorrectEmailOrPassword',
          invalid_credentials_email: 'incorrectEmailOrPassword',
        }),
        status: "error",
        isClosable: true,
      })

    }
  };
  return (
    <UnauthenticatedLayout>
      <Formik
        initialValues={{
          email: '',
          password: '',
        }}
        onSubmit={handleLogin}>
        {(props: FormikProps<LoginForm>): JSX.Element => (
          <form onSubmit={props.handleSubmit}>
            <Heading as="h2" size="xl">
              Login
            </Heading>
            <FormField
              name="email"
              placeholder={t('email')}
              icon="cil-user"
              type="email"
              label="Email"
              helperText="We will never share your email"
            />

            <FormField
              name="password"
              placeholder={t('password')}
              icon="cil-lock-locked"
              type="password"
              label="Password"
            />

            <SubmitButton mt={5} colorScheme="blue" isFullWidth>
              Login
            </SubmitButton>
          </form>
        )}
      </Formik>
    </UnauthenticatedLayout>
  );
};

export default Login;
